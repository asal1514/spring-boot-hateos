package com.sara.products.assemblers;

;


import com.sara.products.model.PackageModel;
import com.sara.products.entity.PackageEntity;
import com.sara.products.entity.PromotionEntity;
import com.sara.products.model.PromotionModel;
import com.sara.products.controller.PackageController;
import com.sara.products.controller.PromotionController;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class PackageModelAssembler  extends RepresentationModelAssemblerSupport<PackageEntity, PackageModel> {

    public PackageModelAssembler() {
        super(PackageController.class, PackageModel.class);
    }

    @Override
    public PackageModel toModel(PackageEntity entity) {
        PackageModel packageModel = instantiateModel(entity);

        packageModel.add(linkTo(
                methodOn(PackageController.class)
                        .getPackageById(entity.getId()))
                .withSelfRel());

        packageModel.setId(entity.getId());
        packageModel.setName(entity.getName());
        packageModel.setTotalInspection(entity.getTotalInspection());
        packageModel.setDueDate(entity.getDueDate());
        packageModel.setMonthlyCost(entity.getMonthlyCost());
        packageModel.setOrder(entity.getPackageOrder());
        packageModel.setDailyCost(entity.getDailyCost());
        packageModel.setInspectionCost(entity.getInspectionCost());
        packageModel.setCreatedAt(entity.getCreatedAt());
        packageModel.setDiscounts(toPromotionModel(entity.getDiscounts()));
        return packageModel;
    }


    @Override
    public CollectionModel<PackageModel> toCollectionModel(Iterable<? extends PackageEntity> entities) {
        CollectionModel<PackageModel> packageModels = super.toCollectionModel(entities);

        packageModels.add(linkTo(methodOn(PackageController.class).getAllPackages()).withSelfRel());

        return packageModels;
    }

    private List<PromotionModel> toPromotionModel(Set<PromotionEntity> discounts) {
        if (discounts.isEmpty())
            return Collections.emptyList();

        return discounts.stream()
                .map(discount -> PromotionModel.builder()
                        .id(discount.getId())
                        .discountCode(discount.getDiscountCode())
                        .creationAt(discount.getCreationAt())
                        .build()
                        .add(linkTo(
                                methodOn(PromotionController.class)
                                        .getDiscountsById(discount.getId()))
                                .withSelfRel()))
                .collect(Collectors.toList());
    }

}
