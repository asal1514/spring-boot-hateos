package com.sara.products.assemblers;


import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.sara.products.model.PackageModel;
import com.sara.products.entity.PackageEntity;
import com.sara.products.entity.PromotionEntity;
import com.sara.products.model.PromotionModel;
import com.sara.products.controller.PackageController;
import com.sara.products.controller.PromotionController;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;


@Component
public class PromotionModelAssembler extends RepresentationModelAssemblerSupport<PromotionEntity, PromotionModel>  {


    public PromotionModelAssembler() {
        super(PromotionController.class, PromotionModel.class);
    }
    @Override
    public PromotionModel toModel(PromotionEntity entity) {
        PromotionModel promotionModel = instantiateModel(entity);
        promotionModel.add(linkTo(
                methodOn(PromotionController.class)
                        .getDiscountsById(entity.getId()))
                .withSelfRel());

        promotionModel.setId(entity.getId());
        promotionModel.setDiscountCode(entity.getDiscountCode());
        promotionModel.setCreationAt(entity.getCreationAt());
        promotionModel.setProducts((toPackageModel(entity.getProducts())));
        return promotionModel;
    }

    @Override
    public CollectionModel<PromotionModel> toCollectionModel(Iterable<? extends PromotionEntity> entities)
    {
        CollectionModel<PromotionModel> promotionModels = super.toCollectionModel(entities);

        promotionModels.add(linkTo(methodOn(PromotionController.class).getAllDiscounts()).withSelfRel());

        return promotionModels;
    }
    private List<PackageModel> toPackageModel(Set<PackageEntity> packages) {
        if (packages.isEmpty())
            return Collections.emptyList();

        return packages.stream()
                .map(product -> PackageModel.builder()
                        .id(product.getId())
                        .name(product.getName())
                        .totalInspection(product.getTotalInspection())
                        .dueDate(product.getDueDate())
                        .monthlyCost(product.getMonthlyCost())
                        .order(product.getPackageOrder())
                        .dailyCost(product.getDailyCost())
                        .inspectionCost(product.getInspectionCost())
                        .createdAt(product.getCreatedAt())
                        .build()
                        .add(linkTo(
                                methodOn(PackageController.class)
                                        .getPackageById(product.getId()))
                                .withSelfRel()))
                .collect(Collectors.toList());
    }
}
