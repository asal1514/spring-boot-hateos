package com.sara.products.controller;

import java.util.List;


import com.sara.products.assemblers.PackageModelAssembler;
import com.sara.products.entity.PackageEntity;
import com.sara.products.model.PackageModel;
import com.sara.products.repository.PackageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PackageController {


	@Autowired
	private PackageModelAssembler packageModelAssembler;

	@Autowired
	private PackageRepository packageRepository;

	@Autowired
	private PagedResourcesAssembler<PackageEntity> pagedResourcesAssembler;


	@GetMapping("/api/packages")
	public ResponseEntity<CollectionModel<PackageModel>> getAllPackages()
	{
		List<PackageEntity> packageEntities = (List<PackageEntity>) packageRepository.findAll();


		return new ResponseEntity<>(
				packageModelAssembler.toCollectionModel(packageEntities),
				HttpStatus.OK);
	}

	@GetMapping("/api/packages/{id}")
	public ResponseEntity<PackageModel> getPackageById(@PathVariable("id") Long id)
	{
		return packageRepository.findById(id)
				.map(packageModelAssembler::toModel)
				.map(ResponseEntity::ok)
				.orElse(ResponseEntity.notFound().build());
	}
	@GetMapping("/api/packages/{id}/im")
	public ResponseEntity<PackageModel> getPackageByImId(@PathVariable("id") Long id)
	{
		return packageRepository.findById(id)
				.map(packageModelAssembler::toModel)
				.map(ResponseEntity::ok)
				.orElse(ResponseEntity.notFound().build());
	}

	//Pageable

	@GetMapping("/api/package-list")
	public ResponseEntity<PagedModel<PackageModel>> getAllPackages(Pageable pageable)
	{
		Page<PackageEntity> packageEntities = packageRepository.findAll(pageable);
		PagedModel<PackageModel> collModel = pagedResourcesAssembler
				.toModel(packageEntities, packageModelAssembler);

		return new ResponseEntity<>(collModel,HttpStatus.OK);
	}

}
