package com.sara.products.controller;


import com.sara.products.assemblers.PromotionModelAssembler;
import com.sara.products.entity.PromotionEntity;
import com.sara.products.model.PromotionModel;
import com.sara.products.repository.PromotionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PromotionController {

    @Autowired
    private PromotionModelAssembler promotionModelAssembler;

    @Autowired
    private PromotionRepository promotionRepository;

    @Autowired
    private PagedResourcesAssembler<PromotionEntity> pagedResourcesAssembler;

//TODO: Autowired should be changed to constructor one and final


    @GetMapping("/api/discounts")
    public ResponseEntity<CollectionModel<PromotionModel>> getAllDiscounts()
    {
        List<PromotionEntity> promotionEntities = (List<PromotionEntity>) promotionRepository.findAll();


        return new ResponseEntity<>(
                promotionModelAssembler.toCollectionModel(promotionEntities),
                HttpStatus.OK);
    }

    @GetMapping("/api/discounts/{id}")
    public ResponseEntity<PromotionModel> getDiscountsById(@PathVariable("id") Long id)
    {
        return promotionRepository.findById(id)
                .map(promotionModelAssembler::toModel)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    //Pageable

    @GetMapping("/api/discount-list")
    public ResponseEntity<PagedModel<PromotionModel>> getAllDiscounts(Pageable pageable)
    {
        Page<PromotionEntity> promotionEntities = promotionRepository.findAll(pageable);
        PagedModel<PromotionModel> collModel = pagedResourcesAssembler
                .toModel(promotionEntities, promotionModelAssembler);

        return new ResponseEntity<>(collModel,HttpStatus.OK);
    }
}
