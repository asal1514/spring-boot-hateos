package com.sara.products.entity;

import lombok.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "discounts")
@Entity
@Table(name="tbl_package")
public class PackageEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Integer totalInspection;
    private String createdAt;
    private String dueDate;
    private Integer packageOrder;
    private Double monthlyCost;
    private Double dailyCost;
    private Double inspectionCost;
    //TODO: Best practice for -ToMany association avoiding to use EAGER fetch type)
    //Best practice for -ToMany association: avoid to use remove(and all) cascade type
    @ManyToMany(cascade={CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(
            name = "package_promotion",
            joinColumns = @JoinColumn(name = "package_id"),
            inverseJoinColumns = @JoinColumn(name = "promotion_id"))
    // Best practice for ManyToMany association
    private Set<PromotionEntity> discounts  = new HashSet<PromotionEntity>();



}
