package com.sara.products.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "products")
@Entity

@Table(name="tbl_promotion")
public class PromotionEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String discountCode;
    private String creationAt;

/*    @ManyToMany(mappedBy = "discounts",fetch = FetchType.EAGER)
    private List<PackageEntity> products;*/
    //Best practice
   @ManyToMany(mappedBy = "discounts")
    private Set<PackageEntity> products  = new HashSet<PackageEntity>();
}
