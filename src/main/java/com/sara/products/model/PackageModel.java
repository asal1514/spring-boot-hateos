package com.sara.products.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Relation(collectionRelation = "packages", itemRelation = "package")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PackageModel
        extends RepresentationModel<PackageModel> {

    private Long id;
    private String name;
    private Integer totalInspection;
    private String createdAt;
    private String dueDate;
    private Integer order;
    private Double monthlyCost;
    private Double dailyCost;
    private Double inspectionCost;
    private List<PromotionModel> discounts;
}
