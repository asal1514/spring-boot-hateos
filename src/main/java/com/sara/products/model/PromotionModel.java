package com.sara.products.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Relation(collectionRelation = "promotions", itemRelation = "promotion")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PromotionModel extends RepresentationModel<PromotionModel> {

    private Long id;
    private String discountCode;
    private String creationAt;
    private List<PackageModel> products;
}
