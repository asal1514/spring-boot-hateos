package com.sara.products.repository;


import com.sara.products.entity.PackageEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PackageRepository extends PagingAndSortingRepository<PackageEntity, Long> {
}
