package com.sara.products.repository;


import com.sara.products.entity.PromotionEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PromotionRepository extends PagingAndSortingRepository<PromotionEntity, Long> {
}
